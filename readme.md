# HTML5 Responsive Images Lunch and Learn 2017-11-08

This project demonstrates responsive images in HTML5. It covers the <picture> element, srcset, selective image preloading and art direction.

There are five HTML files (including index) included that step through examples using device-pixel-ratio, variable-sized images, art direction, and modern image formats. The `5.html` file ties all of these concepts together.

The images used in this demo aren't watermarked with their true size values, nor is there markup provided to indicate what is what. This demo is intended to be used with the Network tab open, watching for images to be selectively fetched.

For continued learning, I think * [Eric Portis](https://ericportis.com/posts/2014/srcset-sizes/) makes some great points, including a case for never actually using the x-descriptor in your srcset.

All demo images courtesy of various photographers, images sourced from [Unsplash](http://unsplash.com)

The [gSlides presentation](https://docs.google.com/presentation/d/1rTE7QJWAkU2A0lGjSOAftTuGnVtPIk-_6Dm5HAy_REQ/edit?usp=sharing) is also available.
